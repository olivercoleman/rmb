import SimpleSchema from 'simpl-schema';

import Content from '../../modules/content';
import Access from 'meteor/ojc:access';


Meteor.users.access = {
  [Access.UNAUTHENTICATED]: [ 'create' ],
  [Access.AUTHENTICATED]: [ ],
  [Access.OWN]: [ 'view', 'update', 'delete' ],
  admin: [ 'create', 'view', 'update', 'delete' ],
}

// Define some default field access rules. It doesn't matter if all the ops don't make sense for a field.
const defaultFieldAccessRules = {
  [Access.AUTHENTICATED]: [ '$setOnInsert' ],
  [Access.OWN]: [ 'view', '$any' ],
  admin: [ 'view', '$any' ],
};


// This schema mostly copied wholesale from https://github.com/aldeed/meteor-collection2#attach-a-schema-to-meteorusers
Meteor.users.schema = {
  username: {
    type: String,
    // For accounts-password, either emails or username is required, but not both. It is OK to make this
    // optional here because the accounts-password package does its own validation.
    // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
    optional: true,
    access: defaultFieldAccessRules,
  },
  emails: {
    type: Array,
    // For accounts-password, either emails or username is required, but not both. It is OK to make this
    // optional here because the accounts-password package does its own validation.
    // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
    optional: true,
    access: defaultFieldAccessRules,
    autoValue: function() {  // https://github.com/aldeed/simple-schema-js/issues/238
      if (!this.isSet && !this.field('emails.0.address').isSet) return [];
    },

    render: value => value.map(v => `${v.address} (${v.verified ? "" : "un"}verified)`),
  },
  "emails.$": {
    type: Object,
  },
  "emails.$.address": {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  "emails.$.verified": {
    type: Boolean,
    defaultValue: false,
  },
  // Use this registered_emails field if you are using splendido:meteor-accounts-emails-field / splendido:meteor-accounts-meld
  // registered_emails: {
  //   type: Array,
  //   optional: true
  // },
  // 'registered_emails.$': {
  //   type: Object,
  //   blackbox: true
  // },
  createdAt: {
    type: Date
  },
  profile: {
    // Don't use profile https://guide.meteor.com/accounts.html#dont-use-profile
    // Sometimes third-party packages use it.
    type: Object,
    blackbox: true,
    optional: true,
  },
  // Make sure this services field is in your schema if you're using any of the accounts packages
  services: {
    type: Object,
    optional: true,
    blackbox: true,
    access: {
      [Access.OWN]: [ 'view' ],
      admin: [ 'view' ],
    },
  },
  // Add `roles` to your schema if you use the meteor-roles package.
  // Option 1: Object type
  // If you specify that type as Object, you must also specify the
  // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
  // Example:
  // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
  // You can't mix and match adding with and without a group since
  // you will fail validation in some cases.
  roles: {
    type: Object,
    optional: true,
    blackbox: true,
    access: {
      admin: [ 'view', '$any' ],
    },
  },
  // Option 2: [String] type
  // If you are sure you will never need to use role groups, then
  // you can specify [String] as the type
  // roles: {
  //     type: Array,
  //     optional: true
  // },
  // 'roles.$': {
  //     type: String
  // },
  // In order to avoid an 'Exception in setInterval callback' from Meteor
  heartbeat: {
    type: Date,
    optional: true,
  },

  // Custom fields below.
  nameFirst: {
    type: String,
    optional: true,
    access: defaultFieldAccessRules,
  },
  nameLast: {
    type: String,
    optional: true,
    access: defaultFieldAccessRules,
  },
};

Meteor.users.attachSchema(new SimpleSchema(Meteor.users.schema));


Content.registerType({
  name: 'users',
  collection: Meteor.users,
});
