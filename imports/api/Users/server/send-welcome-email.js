import { Meteor } from 'meteor/meteor';
import sendEmail from '../../../modules/server/send-email';
import getOAuthProfile from '../../../modules/get-oauth-profile';

export default (options, user) => {
  const OAuthProfile = getOAuthProfile(options, user);
  const emailAddress = OAuthProfile ? OAuthProfile.email : options.email;
  if (emailAddress) {
    const applicationName = Meteor.settings.public.appName;
    const firstName = OAuthProfile ? OAuthProfile.name.first : options.nameFirst;

    return sendEmail({
      to: emailAddress,
      from: `${Meteor.settings.public.appName} ${Meteor.settings.public.appEmails.support}`,
      subject: `[${Meteor.settings.public.appName}] Welcome, ${firstName}!`,
      template: 'welcome',
      templateVars: {
        applicationName: Meteor.settings.public.appName,
        firstName,
        welcomeUrl: Meteor.absoluteUrl(),
      },
    })
    .catch((error) => {
      throw new Meteor.Error('500', `${error}`);
    });
  }
};
