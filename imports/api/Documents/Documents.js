/* eslint-disable consistent-return */

import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

import React from 'react';
import ReactMarkdown from 'react-markdown';

import Content from '../../modules/content';
import Access from 'meteor/ojc:access';

const Documents = new Mongo.Collection('Documents');


Documents.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Documents.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});


Documents.access = {
  [Access.UNAUTHENTICATED]: [ ],
  [Access.AUTHENTICATED]: [ 'create' ],
  [Access.OWN]: [ 'view', 'update', 'delete' ],
  admin: [ 'view', 'update', 'delete' ],
}


Documents.tags = {
  foo: "Foo",
  bar: "Bar",
  baz: "Baz",
  qux: "Qux!",
}

Documents.borderStyles = [
  'solid',
  'dashed',
  'dotted',
  'inset',
  'double',
  'groove',
];


// Define some default field access rules. It doesn't matter if all the ops don't make sense for a field.
// Could really just use '$any' op for Access.OWN rules but for the purposes of demonstration show some specific ops.
const fieldAccessRules = {
  admin: [ 'view', '$any' ],
  [Access.AUTHENTICATED]: [ '$setOnInsert' ],
  [Access.OWN]: [ 'view', '$set', '$inc', '$addToSet', '$push', '$pull', '$pop' ],
};


Documents.schema = {
  userId: {
    type: String,
    label: 'Author',
    autoValue() {
      if (this.isInsert) return Meteor.userId();
    },
    access: { admin: [ 'view', '$any' ], [Access.OWN]: [ 'view' ] },
  },
  createdAt: {
    type: Date,
    label: 'Created',
    autoValue() {
      if (this.isInsert) return new Date();
    },
    access: { admin: [ 'view' ], [Access.OWN]: [ 'view' ] },
  },
  updatedAt: {
    type: Date,
    label: 'Updated',
    autoValue() {
      if (this.isInsert || this.isUpdate) return new Date();
    },
    access: { admin: [ 'view' ], [Access.OWN]: [ 'view' ] },
  },
  title: {
    type: String,
    label: 'Title',
    access: fieldAccessRules,
  },
  body: {
    type: String,
    label: 'Body',
    defaultValue: "",
    access: fieldAccessRules,
    render: v => <ReactMarkdown source={v} />,
  },

  tags: {
    label: 'Tags',
    type: Array,
    optional: true,
    access: fieldAccessRules,
    render: v => v && v.length ? v.join(" | ") : "<No tags>",
  },
  'tags.$': {
    type: String,
    allowedValues: Object.keys(Documents.tags),
    access: fieldAccessRules,
  },

  colour: {
    type: String,
    label: 'Colour',
    defaultValue: '#5fefef',
    access: fieldAccessRules,
  },

  borderStyle: {
    type: String,
    label: 'Border style',
    allowedValues: Documents.borderStyles,
    defaultValue: Documents.borderStyles[0],
    access: fieldAccessRules,
  },

  borderWidth: {
    type: Number,
    label: 'Border width',
    min: 1,
    max: 10,
    defaultValue: 4,
    access: fieldAccessRules,
  },
};
Documents.attachSchema(new SimpleSchema(Documents.schema, { check }));


Content.registerType({
  name: 'documents',
  collection: Documents,
});

export default Documents;
