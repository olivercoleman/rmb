// Add an admin user if no users defined.
if (Meteor.users.find().count() == 0) {
  const adminId = Accounts.createUser({
    //email: "admin@admin.admin",
    username: "admin",
    password: "admin",
    //profile: { name: { first : "Ad", last : "Min" } }
  });
  Roles.addUsersToRoles(adminId, ['admin'], Roles.GLOBAL_GROUP);
}
