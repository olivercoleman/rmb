import React from 'react';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';

import '../both/api';
import App from '../../ui/page/App/App';

import '../../ui/stylesheets/app.scss';

Meteor.startup(() => render(<App />, document.getElementById('react-root')));
