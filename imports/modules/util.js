/**
 * Miscellaneous utitil funtions.
 */


function sleep (ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}


const getUserName = (userOrId) => {
  const user = (typeof userOrId == 'object') ? userOrId : Meteor.users.findOne(userOrId);
  if (!user) return "<User not found>";
  if (user.nameFirst) {
    if (user.nameLast) return `${user.nameFirst} ${user.nameLast}`;
    return user.nameFirst;
  }
  if (user.username) return user.username;
  if (user.emails && user.emails.length) return user.emails[0].address;
  return '<No name>';
}


// Recursively makes a "deep-frozen" copy of the given object or array (or returns the value if it's neither of these).
const deepFreeze = (value) => {
  if (typeof value === 'object') {
    return Object.freeze(_.mapValues(value, v => deepFreeze(v)));
  }
  else if (Array.isArray(value)) {
    return Object.freeze(value.map(v => deepFreeze(v)));
  }
  return value;
}


export { sleep, getUserName, deepFreeze };
