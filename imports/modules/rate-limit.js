import { Meteor } from 'meteor/meteor';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';

export default ({ names, limit, timeRange }) => {
  if (Meteor.isServer) {
    DDPRateLimiter.addRule({
      name(name) { return names.indexOf(name) > -1; },
      connectionId() { return true; },
    }, limit, timeRange);
  }
};
