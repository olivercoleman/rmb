import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import _ from 'lodash';
//import deepMap from 'deep-map';

import rateLimit from './rate-limit';
import handleMethodException from './handle-method-exception';
import Access from 'meteor/ojc:access';



const contentTypes = {};

const defaultRateLimit = {limit: 5, timeRange: 1000};

const defaultMethods = {
  create: true,
  update: true,
  delete: true,
  deleteMultiple: true,
}

const defaultPublications = {
  selectOwn: true,
  selectAny: true,
}


const registerType = ({name, collection, methods, publications}) => {
  contentTypes[name] = collection;

  const schema = collection.simpleSchema().schema();

  const funcs = {
    create: function create(doc) {
      try {
        check(doc, Object);

        // Check general and field access.
        if (!Access.allowed({rules: collection.access, op: 'create', user: this.userId})) throw "Not allowed.";
        if (!Access.modifierAllowed({modifier: {$setOnInsert: doc}, user: this.userId, schema})) throw "Not allowed.";

        // "Clean" and validate document against schema.
        // Clean first to fill in autoValues for validation stage.
        doc = collection.simpleSchema().clean(doc, {mutate: true, extendAutoValueContext: {isInsert: true }});
        collection.simpleSchema().validate(doc);

        return collection.insert(doc);
      } catch (exception) {
        handleMethodException(exception);
      }
    },

    update: function update(id, modifier) {
      try {
        check(id, String);
        check(modifier, Object);

        const doc = collection.findOne(id);
        if (!doc) throw "Invalid document id.";

        if (!Access.allowed({rules: collection.access, op: 'update', item: doc, user: this.userId})) throw "Not allowed.";
        if (!Access.modifierAllowed({modifier, item: doc, user: this.userId, schema})) throw "Not allowed.";

        collection.simpleSchema().validate(modifier, { modifier: true });
        collection.update(id, modifier);
      } catch (exception) {
        handleMethodException(exception);
      }
    },

    'delete': function _delete(id) {
      try {
        check(id, String);

        const doc = collection.findOne(id);
        if (!doc) throw "Invalid document id.";

        if (!Access.allowed({rules: collection.access, op: 'delete', item: doc, user: this.userId})) throw "Not allowed.";

        return collection.remove(id);
      } catch (exception) {
        handleMethodException(exception);
      }
    },

    'deleteMultiple': function _deleteMultiple(ids) {
      try {
        check(ids, [String]);

        // If allowed to delete any.
        if (Access.allowed({rules: collection.access, op: 'delete', user: this.userId})) {
          return collection.remove({_id: {$in: ids}});
        }
        // If allowed to delete own.
        // TODO handle group perms? Would need to include $OR condition for user OR all groups that the user is in.
        else if (Access.allowed({rules: collection.access, op: 'delete', item: {userId: this.userId}, user: this.userId})) {
          if (collection.find({ _id: {$in: ids}, userId: this.userId }).count() < ids.length) {
            throw new Error("Not allowed.");
          }
          return collection.remove({_id: {$in: ids}});
        }
        else {
          throw new Error("Not allowed.");
        }
      } catch (exception) {
        handleMethodException(exception);
      }
    },


    selectOwn: function pubSelectOwn(query, options, collectionPagerId) {
      check(query, Object);
      check(options, Match.Maybe({
        skip: Match.Maybe(Number),
        limit: Match.Maybe(Number),
        sort: Match.Maybe([Match.OneOf(String, [Match.OneOf(String, Number)])]),
        fields: Match.Maybe(Object),
      }));
      check(collectionPagerId, Match.Maybe(String));

      let { skip, limit, sort, fields } = options || {};

      // Restrict to own documents.
      if (name == 'users') query._id = this.userId;
      else query.userId = this.userId;

      // Create a mock document to test for whether access to field is allowed.
      const mockDoc = {userId: this.userId};
      if (fields) {
        const fieldsArray =  Object.keys(fields);
        // Check for white-listing format only.
        _.forOwn(fields, (val, key) => {
          if (val !== 1) throw "'fields' argument may only be of the white-listing form, ie {fieldName: 1, ...}";
        });

        if (!Access.fieldsAllowed({fields: Object.keys(fields), op: 'view', schema, item: mockDoc, user: this.userId})) throw "Not allowed."
      }
      else {
        // Include all fields this user is allowed to access.
        fields = Access.getFieldsForOp({op: 'view', schema, item: mockDoc, user: this.userId})
          .reduce((acc, f) => { return {...acc, [f]: 1}; }, {});
      }

      if (collectionPagerId) {
        if (!options.sort || !options.sort.length) options.sort=[['_id', 'asc']];
        return CollectionPagers.publish(this, collectionPagerId, collection, query, {skip, limit, sort, fields}).pageCursor;
      }

      return collection.find(query, {skip, limit, sort, fields});
    },


    selectAny: function pubSelectAny(query, options, collectionPagerId) {
      check(query, Object);
      check(options, Match.Maybe({
        skip: Match.Maybe(Number),
        limit: Match.Maybe(Number),
        sort: Match.Maybe([Match.OneOf(String, [Match.OneOf(String, Number)])]),
        fields: Match.Maybe(Object),
      }));
      check(collectionPagerId, Match.Maybe(String));

      // If this user is not allowed to view all documents from this collection.
      if (!Access.allowed({rules: collection.access, op: 'view', user: this.userId})) throw "Not allowed.";

      let { skip, limit, sort, fields } = options || {};

      if (fields) {
        const fieldsArray =  Object.keys(fields);
        // Check for white-listing format only.
        _.forOwn(fields, (val, key) => {
          if (val !== 1) throw "'fields' argument may only be of the white-listing form, ie {fieldName: 1, ...}";
        });

        if (!Access.fieldsAllowed({fields: Object.keys(fields), op: 'view', schema, user: this.userId})) throw "Not allowed."
      }
      else {
        // Include all fields this user is allowed to access.
        fields = Access.getFieldsForOp({op: 'view', schema, user: this.userId})
          .reduce((acc, f) => { return {...acc, [f]: 1}; }, {});
      }

      if (collectionPagerId) {
        if (!options.sort || !options.sort.length) options.sort=[['_id', 'asc']];
        return CollectionPagers.publish(this, collectionPagerId, collection, query, {skip, limit, sort, fields}).pageCursor;
      }

      return collection.find(query, {skip, limit, sort, fields});
    },
  }


  methods = _.assign(methods || {}, defaultMethods);
  for (let methodType of Object.keys(defaultMethods)) {
    if (methods[methodType]) {
      const methodName = `${name}.${methodType}`;
      const spec = methods[methodType];

      Meteor.methods({ [methodName]: funcs[methodType] });

      const limitRule = typeof spec === 'object' && spec.rateLimit ? spec.rateLimit : defaultRateLimit;
      if (limitRule) rateLimit({names: [methodName], ...limitRule});
    }
  }


  if (Meteor.isServer) {
    publications = _.assign(publications || {}, defaultPublications);
    for (let pubType of Object.keys(defaultPublications)) {
      if (publications[pubType]) {
        const pubName = `${name}.${pubType}`;
        const spec = publications[pubType];

        Meteor.publish(pubName, funcs[pubType]);

        const limitRule = typeof spec === 'object' && spec.rateLimit ? spec.rateLimit : defaultRateLimit;
        if (limitRule) rateLimit({names: [pubName], ...limitRule});
      }
    }
  }
}


const getTypes = () => Object.keys(contentTypes);

const getTypeCollection = (type) => contentTypes[type];


export { registerType, getTypes, getTypeCollection, defaultRateLimit, defaultMethods, defaultPublications };
