import JSURL from "jsurl";
import _ from 'lodash';
import immutable from 'immutable';

const pushLocationHashParams = (location, history, params) => {
  if (typeof params != 'object') throw "Hash params must be an object";
  const parsed = JSURL.parse(location.hash.substr(1));
  let newParams = {
    ...parsed,
    ...params
  };
  // Filter out null/undefined/empty array values
  newParams = _.pickBy(newParams, (value, key) => value !== null && value !== undefined)
  history.push({hash: JSURL.stringify(newParams)});
}


const setLocationHashParams = (location, history, params) => {
  // Filter out null/undefined values
  params = _.pickBy(params, (value, key) => value !== null && value !== undefined)
  history.push({hash: JSURL.stringify(params)});
}


const parseHashParams = (location, defaults) => {
  // Parse location query params, including defaults if given.
  let parsed = {
    ...(defaults || {}),
    ...JSURL.parse(location.hash.substr(1))
  }
  // For values that look numeric return a number.
  //parsed = _.mapValues(parsed, value => ""+parseFloat(value) == value ? parseFloat(value) : value);
  return parsed;
}


const openInNewWindow = (address, focus) => {
  const loc = window.location;
  const url = `${loc.protocol}//${loc.hostname}${loc.port ? ':'+loc.port: ''}/${address}`;
  const win = window.open(url, '_blank');
  focus && win.focus();
}


const toggleSet = (set, item) => {
  if (set.has(item)) return set.delete(item);
  return set.add(item);
}


export { parseHashParams, setLocationHashParams, pushLocationHashParams, openInNewWindow, toggleSet }
