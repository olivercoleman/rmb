import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import { Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import Access from 'meteor/ojc:access';

import Content from '../../../modules/content';


const AuthenticatedNavigation = ({ name, history }) => {
  return (
    <div>
      <Nav>
        <LinkContainer to="/documents">
          <NavItem eventKey={1} href="/documents">Documents</NavItem>
        </LinkContainer>

        { Access.allowed({role: 'admin'}) &&
          <NavDropdown eventKey={3} title={name} id="admin-Content-nav-dropdown">
            { Content.getTypes()
              .filter(contentType => Access.allowed({rules: Content.getTypeCollection(contentType).access, op: 'view'}))
              .map((contentType, index) =>
                <LinkContainer to={`/admin/content/${contentType}`} key={contentType}>
                  <NavItem eventKey={`3.${index}`} href={`/admin/content/${contentType}`}>{contentType}</NavItem>
                </LinkContainer>
              )
            }
          </NavDropdown>
        }
      </Nav>
      <Nav pullRight>
        <NavDropdown eventKey={2} title={name} id="user-nav-dropdown">
          <LinkContainer to="/profile">
            <NavItem eventKey={2.1} href="/profile">Profile</NavItem>
          </LinkContainer>
          <MenuItem divider />
          <MenuItem eventKey={2.2} onClick={() => history.push('/logout')}>Logout</MenuItem>
        </NavDropdown>
      </Nav>
    </div>
  );
}

AuthenticatedNavigation.propTypes = {
  name: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(AuthenticatedNavigation);
