import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import classNames from 'classnames';
import {Collapse} from 'react-collapse';
import { DateTime } from 'luxon';
import ReactMarkdown from 'react-markdown';

import DocumentsCollection from '../../../api/Documents/Documents';
import Loading from '../../misc/Loading/Loading';
import ExpandControl from '../../misc/ExpandControl/ExpandControl';
import MultiView from '../../misc/MultiView/MultiView.jsx';

import './DocumentsList.scss';


class DocumentsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
    }
    autoBind(this);
  }


  handleRemove(documentId) {
    if (confirm('Are you sure? This is permanent!')) {
      Meteor.call('documents.delete', documentId, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert('Document deleted!', 'success');
        }
      });
    }
  }


  handleNew() {
    const { history } = this.props;

    const doc = {
      title: "My new document",
      body: "",
    }
    Meteor.call('documents.create', doc, (error, docId) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        history.push('/documents/' + docId);
      }
    });
  }


  render() {
    const { loading, documents, match, history } = this.props;
    const { expanded } = this.state;

    const fields = [
      {
        field: "title",
        render: (v, doc) => <Link to={`${match.url}/${doc._id}`}>{v}</Link>,
      },
      {
        field: "body",
        render: v => <ReactMarkdown source={v} />,
      },
      {
        field: "createdAt",
        render: v => <div>Posted: {DateTime.fromJSDate(v).toLocaleString(DateTime.DATETIME_MED)}</div>,
      },
      {
        field: "updatedAt",
        render: (v, doc) => doc.createdAt != doc.updatedAt && <div>Last updated: {DateTime.fromJSDate(v).toLocaleString(DateTime.DATETIME_MED)}</div>,
      },
    ];


    return (!loading ? (
      <div className="Documents">
        <div className="page-header clearfix">
          <h4 className="pull-left">Documents <ExpandControl expanded={expanded} onToggle={expanded => this.setState({expanded})} size="150%" /></h4>
          <Button className="btn-success pull-right" onClick={this.handleNew}>Add Document</Button>
        </div>
        <Collapse isOpened={expanded}>
          <MultiView
            multiviewId="documents-list"
            collection={DocumentsCollection}
            subscriptionName="documents.selectOwn"
            history={history}
            location={location}
            fields={fields}
            showPager={true}
            initialSorts={[['createdAt', -1]]}
            initialLimit={10}
          />
        </Collapse>
      </div>
    ) : <Loading />);
  }
}


DocumentsList.propTypes = {
  loading: PropTypes.bool.isRequired,
  documents: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};


export default withTracker(() => {
  const subscription = Meteor.subscribe('documents.selectOwn',
    {},
    {
      sort: ['userId', ['name', 'desc']],
    }
  );

  return {
    loading: !subscription.ready(),
    documents: DocumentsCollection.find().fetch(),
  };
})(DocumentsList);
