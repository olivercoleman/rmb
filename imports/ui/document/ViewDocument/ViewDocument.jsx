import React from 'react';
import PropTypes from 'prop-types';
import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import { DateTime } from 'luxon';
import classNames from 'classnames';

import Documents from '../../../api/Documents/Documents';
import Loading from '../../misc/Loading/Loading';
import EditInline from '../../misc/EditInline/EditInline.jsx';

import './ViewDocument.scss';


const handleRemove = (document, isNew, history) => {
  if (isNew || confirm('Are you sure? This is permanent!')) {
    Meteor.call('documents.delete', document._id, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        !isNew && Bert.alert('Document deleted!', 'success');
        history.push('/documents');
      }
    });
  }
};


const ViewDocument = ({document, loading, history}) => {
  if (loading || !document) return <Loading />;

  // It's new if it's less than 10 seconds old.
  const isNew = DateTime.fromJSDate(document.createdAt).diffNow().as('seconds') > -10;

  return (<div className="ViewDocument" style={{ borderColor: document.colour, borderStyle: document.borderStyle, borderWidth: document.borderWidth }}>
    <div className="page-header clearfix">
      <h4 className="pull-left">
        <EditInline doc={document} field="title" updateMethod="documents.update" inputType={EditInline.types.textfield} required={true} />
      </h4>

      <ButtonToolbar className="pull-right">
        <ButtonGroup bsSize="small">
          <Button onClick={() => handleRemove(document, isNew, history)} className="text-danger">
            { isNew ? 'Cancel' : 'Delete' }
          </Button>
        </ButtonGroup>
      </ButtonToolbar>
    </div>

    <EditInline doc={document} field="body" updateMethod="documents.update" inputType={EditInline.types.wysiwyg} required={true} />

    <div className="colour">
      <h4>Border colour:</h4>
      <EditInline doc={document} field="colour" updateMethod="documents.update" inputType={EditInline.types.colour} />
    </div>

    <div className="style">
      <h4>Border style:</h4>
      <EditInline doc={document} field="borderStyle" updateMethod="documents.update" inputType={EditInline.types.select}
        options={Documents.borderStyles.map(style => { return { value: style, label: style } } )}
        valueKey='value' labelKey='label'
        isMulti={false}
        emptyValue="Set..."
      />
    </div>

    <div className="width">
      <h4>Border width:</h4>
      <EditInline doc={document} field="borderWidth" updateMethod="documents.update" inputType={EditInline.types.integer} min={1} max={10} />
    </div>

    <div className="tags">
      <h4>Tags:</h4>
      <EditInline doc={document} field="tags" updateMethod="documents.update" inputType={EditInline.types.select}
        options={Object.keys(Documents.tags).map(key => { return { value: key, label: Documents.tags[key] } } )}
        valueKey='value' labelKey='label'
        isMulti={true}
        emptyValue="Set..."
      />
    </div>

    <div className="timestamps">
      { ["Created", "Updated"].map(field =>
        <span className={classNames("timestamp", field)} key={field}>
          <h5>{field}</h5>: { DateTime.fromJSDate(document[`${field.toLowerCase()}At`]).toLocaleString(DateTime.DATETIME_MED) }
        </span>
      )}
    </div>

  </div>)
};


ViewDocument.propTypes = {
  loading: PropTypes.bool.isRequired,
  document: PropTypes.object,
};


export default withTracker(({match, history}) => {
  const subscription = Meteor.subscribe('documents.selectOwn', {_id: match.params._id});

  return {
    loading: !subscription.ready(),
    document: Documents.findOne({_id: match.params._id}),
    history,
  };
})(ViewDocument);
