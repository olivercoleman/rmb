/* eslint-disable no-underscore-dangle */

import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import base64ToBlob from 'b64-to-blob';
import { Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import _ from 'lodash';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Bert } from 'meteor/themeteorchef:bert';
import { withTracker } from 'meteor/react-meteor-data';
import classNames from 'classnames';
import PasswordMask from 'react-password-mask';

import EditInline from '../../misc/EditInline/EditInline.jsx';
import AccountPageFooter from '../AccountPageFooter/AccountPageFooter';
import Loading from '../../misc/Loading/Loading';

import './Profile.scss';


class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: '',
      newPassword: '',
      newPasswordVerify: '',
    };
    autoBind(this);
  }
  

  getUserType(user) {
    const userToCheck = user;
    delete userToCheck.services.resume;
    const service = Object.keys(userToCheck.services)[0];
    return service === 'password' ? 'password' : 'oauth';
  }


  render() {
    return (
      <div className="Profile">
        <h4 className="page-header">Edit Profile</h4>

        {this.renderProfileForm()}

        <AccountPageFooter>
          <Button bsStyle="danger" onClick={this.handleDeleteAccount}>Delete My Account</Button>
        </AccountPageFooter>
      </div>
    );
  }


  renderProfileForm() {
    const { loading, user } = this.props;
    if (loading) return (<Loading />);
    const type = this.getUserType(user);
    if (type == 'password') return this.renderPasswordUser(user);
    return this.renderOAuthUser(user);
  }


  renderOAuthUser(loading, user) {
    return !loading ? (
      <div className="OAuthProfile">
        {Object.keys(user.services).map(service => (
          <div key={service} className={`LoggedInWith ${service}`}>
            <img src={`/${service}.svg`} alt={service} />
            <p>{`You're logged in with ${_.capitalize(service)} using the email address ${user.services[service].email}.`}</p>
            <Button
              className={`btn btn-${service}`}
              href={{
                facebook: 'https://www.facebook.com/settings',
                google: 'https://myaccount.google.com/privacy#personalinfo',
                github: 'https://github.com/settings/profile',
              }[service]}
              target="_blank"
            >
              Edit Profile on {_.capitalize(service)}
            </Button>
          </div>
        ))}
      </div>) : <div />;
  }


  renderPasswordUser(user) {
    const { oldPassword, newPassword, newPasswordVerify } = this.state;

    return (
      <div className="PasswordProfile">
        <FormGroup>
          <ControlLabel>Username</ControlLabel>
          <EditInline doc={user} field="username" updateMethod="users.update" inputType={EditInline.types.textfield} required={true}/>
        </FormGroup>
        <FormGroup>
          <ControlLabel>First name</ControlLabel>
          <EditInline doc={user} field="nameFirst" updateMethod="users.update" inputType={EditInline.types.textfield} required={true}/>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Last name</ControlLabel>
          <EditInline doc={user} field="nameLast" updateMethod="users.update" inputType={EditInline.types.textfield} required={true}/>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Email address</ControlLabel>
          <EditInline doc={user} field="emails.0.address" updateMethod="users.update" inputType={EditInline.types.textfield}
            required={true}
            validationFuncs={ [ v => Meteor.users.schema['emails.$.address'].regEx.test(v) ? true : "Please enter a valid email address." ] }
          />
        </FormGroup>

        <div className="password">
          <h4>Change password</h4>
          <FormGroup>
            <ControlLabel>Current</ControlLabel>
            <PasswordMask
              id="currentPassword" name="currentPassword"
              placeholder="Enter current password"
              value={oldPassword}
              onChange={e => this.setState({oldPassword: e.target.value})}
              className="password-mask"
              showButtonContent="👀" hideButtonContent="👀"
            />
          </FormGroup>
          <FormGroup>
            <ControlLabel>New</ControlLabel>
            <PasswordMask
              id="newPassword" name="newPassword"
              placeholder="Enter new password"
              value={newPassword}
              onChange={e => this.setState({newPassword: e.target.value})}
              className="password-mask"
              showButtonContent="👀" hideButtonContent="👀"
            />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Verify new</ControlLabel>
            <PasswordMask
              id="newPasswordVerify" name="newPasswordVerify"
              placeholder="Verify new password"
              value={newPasswordVerify}
              onChange={e => this.setState({newPasswordVerify: e.target.value})}
              className="password-mask"
              showButtonContent="👀" hideButtonContent="👀"
            />
          </FormGroup>

          {oldPassword && newPassword && newPasswordVerify &&
            <Button
              className={classNames("update-password", newPassword && newPasswordVerify && newPassword != newPasswordVerify ? 'btn-danger' : 'btn-success')}
              disabled={newPassword != newPasswordVerify}
              onClick={this.handlePasswordChange}
            >
              { newPassword != newPasswordVerify ? "New passwords do not match" : "Update password" }
            </Button>
          }
        </div>
      </div>
    );
  }


  handlePasswordChange() {
    const { oldPassword, newPassword, newPasswordVerify } = this.state;
    if (oldPassword && newPassword && newPasswordVerify && newPassword == newPasswordVerify) {
      Accounts.changePassword(oldPassword, newPassword, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert("Password updated.", "success");
          this.setState({oldPassword: '', newPassword: '', newPasswordVerify: '' });
        }
      });
    }
  }


  handleDeleteAccount() {
    if (confirm('Are you sure? This will permanently delete your account and all of its data.')) {
      Meteor.call('users.deleteAccount', (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert('Account deleted!', 'success');
        }
      });
    }
  }
}

Profile.propTypes = {
  loading: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
};

export default withTracker(() => {
  const subscription = Meteor.subscribe('users.selectOwn', {_id: Meteor.userId()});

  return {
    loading: !subscription.ready(),
    user: Meteor.user()
  };
})(Profile);
