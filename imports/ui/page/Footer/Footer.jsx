import React from 'react';
import { Link } from 'react-router-dom';
import { Grid } from 'react-bootstrap';
import { DateTime } from 'luxon';

import './Footer.scss';

const Footer = () => (
  <footer className="Footer">
    <p>&copy; {DateTime.local().year} {Meteor.settings.public.appName}</p>
    <ul>
      <li><Link to="/terms">Terms<span className="hidden-xs"> of Service</span></Link></li>
      <li><Link to="/privacy">Privacy<span className="hidden-xs"> Policy</span></Link></li>
    </ul>
  </footer>
);

Footer.propTypes = {};

export default Footer;
