import React from 'react';
import ReactDOM from 'react-dom';
import reactCSS from 'reactcss'
import { PhotoshopPicker } from 'react-color';
import EIBase from './EIBase.jsx';

import './EditInline.scss';

export default class EIColour extends EIBase {
  constructor(props) {
    super(props);
		this.timeoutId = false;
  }

  handleChangeEvent = (event) => {
    // According to the spec the "input event is fired at the input element
    // every time the color changes. The change event is fired when the user
    // dismisses the color picker" but Chrome fires both events
    // every time the colour changes. This function prevents too many
    // requests being sent to the server.
    window.clearTimeout(this.timeoutId);
    this.timeoutId = window.setTimeout(() => {
      this.finishEditing();
    }, 1000);
  }

  renderEditingComponent = () => {
    return this._renderComponent(true);
  }

  renderNormalComponent = () => {
    return this._renderComponent(false);
  }

  _renderComponent = (editing) => {
    const { value, shouldBlockWhileLoading, editProps } = this.props;
    const { loading, newValue } = this.state;
    const disabled = (!!this.props.disabled) || ((!!shouldBlockWhileLoading) && (!!loading));

    return (<div className="edit-inline-color-container">
       <div style={{backgroundColor: value}} className="edit-inline-color" />

       {editing && <PhotoshopPicker
         color={newValue}
         onChange={color => this.valueChanged(color.hex)}
         onAccept={this.finishEditing}
         onCancel={this.cancelEditing}
         ref="input"
         {...editProps}
       /> }
    </div>);
  }

  hideButtons = () => true;
}
