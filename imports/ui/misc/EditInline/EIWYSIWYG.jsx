import React from 'react';
import ReactDOM from 'react-dom';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { draftToMarkdown, markdownToDraft } from 'markdown-draft-js';
import { convertToRaw, convertFromRaw, EditorState, ContentState } from 'draft-js';

import EIBase from './EIBase.jsx';

export default class EIWYSIWYG extends EIBase {
  constructor(props) {
    super(props);
    this.state.editorState = this.markdownToEditorState(this.props.value || this.props.emptyValue || "Set me.");
  }

  markdownToEditorState = (md) => EditorState.createWithContent(convertFromRaw(markdownToDraft(md)));

  beforeStart = () => {
    this.setState({
      editorState: this.markdownToEditorState(this.props.value)
    });
  }

  afterFinish = (currentValue) => {
    const value = currentValue || this.props.emptyValue || "Set me.";
    this.setState({
      editorState: this.markdownToEditorState(value)
    });
  }

  renderEditingComponent = () => {
    return this._renderComponent(true);
  };

  renderNormalComponent = () => {
    return this._renderComponent(false);
  };

  _renderComponent = (editing) => {
    const viewOnly = !editing || this.state.loading || this.props.disabled;
    return (
      <Editor
        editorState={this.state.editorState}
        onEditorStateChange={this.onEditorStateChange}
        readOnly={viewOnly}
        toolbarHidden={viewOnly}
        ref="input"
        {...this.props.editProps}
      />
    )
  }

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState,
    });

    // Wait a bit since last keytroke before performing markdown conversion and validation.
    if (this.updateNewValueTimeoutHandle) Meteor.clearTimeout(this.updateNewValueTimeoutHandle);
    this.updateNewValueTimeoutHandle = Meteor.setTimeout(
      () => {
        const rawContentState = convertToRaw(editorState.getCurrentContent());
        const markdown = draftToMarkdown(rawContentState);
        this.valueChanged(markdown);
      },
      500
    );
  };


  componentDidUpdate = (prevProps, prevState) => {
    if (this.state.editing && !prevState.editing) {
      ReactDOM.findDOMNode(this.refs.input).focus();
    }
  };
}
