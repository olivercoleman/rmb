import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { ButtonToolbar, ButtonGroup, Button, Alert, DropdownButton, SplitButton, MenuItem, Modal, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import _ from 'lodash';
import classNames from 'classnames';


export default class MultiViewItem extends React.Component {
  render() {
    const { item, fields, isSelected, onClick } = this.props;

    return (
      <div className={classNames("multiview-item", isSelected && "multiview-item-selected")} >
        {fields.filter(field => field.render !== false).map(field =>
          <div
            className={classNames("multiview-field", field.field.replace(/\./g, '-'), field.classes)}
            title={field.label}
            key={item._id + field.label + field.field}
            onClick={!field.notSelectable ? onClick : null}
          >
            {field.render ? field.render(_.get(item, field.field), item) : _.get(item, field.field)}
          </div>
        )}
      </div>
    );
  }
}
