import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { ButtonToolbar, ButtonGroup, Button, Alert, DropdownButton, SplitButton, MenuItem } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import _ from 'lodash';
import deepMap from 'deep-map';

import Loading from '../Loading/Loading.jsx';
import MultiViewItem from './MultiViewItem';


class MultiViewItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lastSelected: null,
    }
    autoBind(this);
  }


  render() {
    const { loading, fields, items, onSelectedChange, selectedItemIds } = this.props;
    if (loading) return <Loading />;

    return (<div className="multiview-items">
      {items.map(item =>
        <MultiViewItem
          item={item}
          fields={fields}
          key={item._id}
          onClick={e => !!onSelectedChange && this.handleToggleSelected(item._id, e) }
          isSelected={!!selectedItemIds && selectedItemIds.has(item._id)}
          title={!!onSelectedChange ? "Click to select. Hold down shift to (de)select multiple items." : null}
        />
      )}
    </div>)
  }


  // Handle multiple select of contiguous items when holding down shift key.
  handleToggleSelected(itemId, e) {
    const { lastSelected } = this.state;
    const { items, onSelectedChange, selectedItemIds } = this.props;

    const newSelectedStatus = !selectedItemIds.has(itemId);

    // If shift was pressed, and there is a previously selected item, and it's not this one.
    if (e.shiftKey && lastSelected && lastSelected != itemId) {
      // Deselect text selected by holding shift.
      if (document.selection) document.selection.empty();
      else if (window.getSelection) window.getSelection().removeAllRanges();

      let first;
      const changedItemIds = [];
      for (let item of items) {
        // If we haven't yet found the first item in the range.
        if (!first) {
          // See if this item is the first.
          if (item._id == itemId) first = itemId;
          else if (item._id == lastSelected) first = lastSelected;
        }

        // If we've found the first in the range, start setting selected status.
        if (first) {
          changedItemIds.push(item._id);
        }

        // If this isn't the first in the range, see if it's the last and we should stop.
        if (first && first != item._id && (item._id == itemId || item._id == lastSelected)) break;
      }

      onSelectedChange(changedItemIds, newSelectedStatus);
    }
    else {
      onSelectedChange([itemId], newSelectedStatus);
    }

    this.setState({lastSelected: itemId});
  }
}


export default createContainer(({ collection, subscriptionName, fields,
      skip, limit, filters, sorts, collectionPagerId,
      onSelectedChange, selectedItemIds,
      selectorAlterations }) =>
{
  const options = {sort: sorts, skip, limit};

  let selector = {...filters};
  if (selector.$text) {
    selector.$text = { $search: selector.$text };
  }
  
  // Convert string values to schema types where appropriate.
  selector = _.mapValues(selector, (val, field) => {
    if (field == '$text') return val;
    const type = fields.find(f => f.field == field).filter.type;
    return deepMap(val, v => type == Date ? new Date(v) : type(v));
  });

  const selectorAdjusted = {};
  _.forOwn(selector, (value, field) => {
    if (Array.isArray(value)) selectorAdjusted[field] = {$in: value};

    else if (value === false) {
      if (!selectorAdjusted.$and) selectorAdjusted.$and = [];
      selectorAdjusted.$and.push({ $or: [ {[field]: false}, {[field]: {$exists: false}} ] });
    }
    else {
      return selectorAdjusted[field] = value;
    }
  });

  if (selectorAlterations) selectorAlterations(selectorAdjusted);

  const subscription = Meteor.subscribe(subscriptionName, selectorAdjusted, options, collectionPagerId);

  return {
    items: CollectionPagers.getDocuments(collectionPagerId, collection),
    loading: !subscription.ready(),
    fields,
    onSelectedChange,
    selectedItemIds,
  };
}, MultiViewItems);
