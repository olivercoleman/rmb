import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { ButtonToolbar, ButtonGroup, Button, Alert, DropdownButton, SplitButton, MenuItem, FormControl } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import _ from 'lodash';
import classNames from 'classnames';
import Select from 'react-select';
//import 'react-select/dist/react-select.css';
import DateTime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';

import { Bert } from 'meteor/themeteorchef:bert';
import { Set } from 'immutable';
import update from 'update-immutable';
import SimpleSchema from 'simpl-schema';

import util from '../../util';
import Loading from '../Loading/Loading.jsx';
import Pager from '../Pager/Pager.jsx';
import MultiViewItems from './MultiViewItems.jsx';

import './multiview.scss';


class MultiView extends React.Component {
  constructor(props) {
    super(props);

    const { filters, sorts, limit, skip } = util.parseHashParams(this.props.location);

    this.state = {
      textSearchTemp: filters && filters.$text || '',
    }

    // const initialLocationParams = {}
    // if (!filters) initialLocationParams.filters = this.props.initialFilters;
    // if (!sorts) initialLocationParams.sorts = this.props.initialSorts;
    // if (typeof limit == 'undefined') initialLocationParams.limit = this.props.initialLimit;
    // if (typeof skip == 'undefined') initialLocationParams.skip = this.props.initialSkip;
    // console.log(initialLocationParams);
    // if (Object.keys(initialLocationParams).length) {
    //   util.pushLocationHashParams(this.props.location, this.props.history, initialLocationParams);
    // }

    autoBind(this);
  }


  handleTextSearchChange(event) {
    const { history, location } = this.props;
    const textSearchTemp = event.target.value;
    this.setState({textSearchTemp});

    // Wait a second since last keytroke before performing text search and updating history.
    if (this.updateSearchTimeoutHandle) Meteor.clearTimeout(this.updateSearchTimeoutHandle);
    this.updateSearchTimeoutHandle = Meteor.setTimeout(
      () => {
        const { filters } = util.parseHashParams(location, { filters: {} });
        if (textSearchTemp == '') delete(filters.$text);
        else filters.$text = textSearchTemp;
        util.pushLocationHashParams(location, history, { filters, skip: 0 });
      },
      1000
    );
  }


  render() {
    const { multiviewId, collection, subscriptionName, history, location, fields,
      enableTextSearch, textSearchPlaceholder,
      extraControls, onSelectedChange, selectedItemIds,
      selectorAlterations, className,
      showPager, showFilters, showHeaders,
      initialLimit, initialSkip, initialFilters, initialSorts } = this.props;

    const { skip, limit, filters, sorts } = util.parseHashParams(location, {
      skip: initialSkip,
      limit: initialLimit,
      filters: initialFilters,
      sorts: initialSorts,
    });

    const sortsObj = _.fromPairs(sorts);

    const collectionPagerId = multiviewId +
      _.transform(filters, (result, value, field) => result + `-f:${field}:${value}`, '') +
      '-' + sorts.map(s => `s:${s[0]}:${String(s[1]).substr(0,1)}`, '').join('-');

    return (
      <div className={classNames(className, "MultiView")}>
        <div className="multiview-controls">
          { !!showPager && <Pager
            collectionPagerId={collectionPagerId}
            skip={skip}
            limit={limit}
            onPageChange={ skip => util.pushLocationHashParams(location, history, {skip}) }
            onLimitChange= { limit => util.pushLocationHashParams(location, history, {limit}) }
          /> }

          { !!showFilters && fields.filter(field => !!field.filter).map(field => (
            <div className="filter-wrapper" key={field.field}>
              { this.renderFilter(field, filters, location, history) }
            </div>
          )) }

          { enableTextSearch && <FormControl
            type="text"
            className="text-filter"
            value={this.state.textSearchTemp || ''}
            placeholder={textSearchPlaceholder || "Text search"}
            title='Use double quotes for an exact phrase search and for terms containing punctuation, eg "00-40.64". Terms can be excluded by negating them, eg Newcastle -East will include items with Newcastle in the text but not East.'
            onChange={this.handleTextSearchChange}
          /> }

          { extraControls }
        </div>

        <div className="multiview-content">
          { !!showHeaders && <div className="multiview-headers">
            <div className="multiview-item">
              {fields.filter(field => field.render !== false).map(field =>
                <div
                  className={classNames("multiview-field", field.field.replace(/\./g, '-'), field.sortable && 'sortable', field.classes)}
                  key={field.label || field.field}
                >
                  <div
                    className={"multiview-field-inner"}
                    title={field.description}
                    onClick={() => field.sortable && this.updateSorts(sorts, field.field)}
                  >
                    <span>{field.label || field.field}</span>
                    {field.sortable &&
                      <span className={classNames('fa',
                        sortsObj[field.field] == 'desc' && 'fa-sort-desc',
                        sortsObj[field.field] == 'asc' && 'fa-sort-asc',
                        !sortsObj[field.field] && 'fa-sort'
                      )} />
                    }
                  </div>
                </div>
              )}
            </div>
          </div> }

          <MultiViewItems
            collection={collection}
            subscriptionName={subscriptionName}
            fields={fields}
            skip={skip}
            limit={limit}
            filters={filters}
            sorts={sorts}
            collectionPagerId={collectionPagerId}
            selectorAlterations={selectorAlterations}
            onSelectedChange={onSelectedChange ? this.handleSelectedChange : null}
            selectedItemIds={selectedItemIds}
          />
      </div>
    </div>)
  }


  renderFilter(field, filters, location, history) {
    const filter = field.filter;

    if (filter.options) {
      const options = typeof field.filter.options[0] == 'object' ? field.filter.options :
        field.filter.options.map(o => { return {
          [field.filter.valueKey || 'value']: o,
          [field.filter.labelKey || 'label']: o,
        }});

      let value;
      if (filters[field.field] && (!field.filter.multi || filters[field.field].length)) {
        if (field.filter.multi) {
          value = options.filter(o => filters[field.field].includes(o[field.filter.valueKey || 'value']));
        }
        else {
          value = options.find(o => o[field.filter.valueKey || 'value'] == filters[field.field]);
        }
      }

      return (<Select
        className="mv-select"
        classNamePrefix="mv-select"
        placeholder={field.label || field.field}
        options={options}
        valueKey={field.filter.valueKey}
        labelKey={field.filter.labelKey}
        isMulti={!!field.filter.multi}
        value={value}
        onChange={val => this.updateFilters(filters, field, val)}
        key={field.field}
      />);
    }

    if ([String, Number, SimpleSchema.Integer].includes(filter.type)) {
      return (<FormControl
        type={field.filter.type == String ? 'text' : 'number'}
        value={filters[field.field] || ""}
        placeholder={field.label || field.field}
        title={field.label || field.field}
        onChange={e => this.updateFilters(filters, field, e.target.value)}
      />);
    }

    if (field.filter.type == Date) {
      return (<div className="date-range">
        <DateTime
          dateFormat={"YYYY-MM-DD"}
          timeFormat={"HH:mm:ss"}
          value={this.state[`datetimeFilterTempFrom_${field.field}`] || _.get(filters, field.field + '.$gte')}
          onChange={ moment => this.setState({[`datetimeFilterTempFrom_${field.field}`]: moment}) }
          onBlur={ moment => {
            this.setState({[`datetimeFilterTempFrom_${field.field}`]: null});
            util.pushLocationHashParams(location, history, {
              filters: update(filters,
                !moment || !moment.unix ? (
                  ! _.get(filters, field.field + '.$lte') ?
                  {$unset: [field.field]}
                  :
                  {[field.field]: {$unset: '$gte'}}
                )
                :
                {[field.field]: {$$gte: {$set: moment.unix() * 1000}}}
              )
            });
          } }
          inputProps={{placeholder: (field.label || field.field) + " from"}}
        />

        <div className="date-range-separator" />

        <DateTime
          dateFormat={"YYYY-MM-DD"}
          timeFormat={"HH:mm:ss"}
          value={this.state[`datetimeFilterTempTo_${field.field}`] || _.get(filters, field.field + '.$lte')}
          onChange={ moment => this.setState({[`datetimeFilterTempTo_${field.field}`]: moment}) }
          onBlur={ moment => {
            this.setState({[`datetimeFilterTempTo_${field.field}`]: null});
            util.pushLocationHashParams(location, history, {
              filters: update(filters,
                !moment || !moment.unix ?(
                  ! _.get(filters, field.field + '.$gte') ?
                  {$unset: [field.field]}
                  :
                  {[field.field]: {$unset: '$lte'}}
                )
                :
                {[field.field]: {$$lte: {$set: moment.unix() * 1000}}}
              )
            });
          } }
          inputProps={{placeholder: (field.label || field.field) + " to"}}
        />
      </div>);
    }
  }


  updateFilters(filters, field, val) {
    filters = {...filters};

    // If this is a Select.
    if (field.filter.options) {
      if (field.filter.multi) {
        if (!Array.isArray(val)) val = [val];
        val = val.map(v => v[field.filter.valueKey || 'value']);
        if (!val.length) delete(filters[field.field]);
        else filters[field.field] = val;
        util.pushLocationHashParams(this.props.location, this.props.history, { filters, skip: 0 });
      }
      else {
        if (typeof val == 'undefined' || val === null || val.length == 0) delete(filters[field.field]);
        else filters[field.field] = val[field.filter.valueKey || 'value'];
        util.pushLocationHashParams(this.props.location, this.props.history, { filters, skip: 0 });
      }
    }
    else {
      if (typeof val == 'undefined' || val === null || val.length === 0) delete(filters[field.field]);
      else filters[field.field] = val;
      util.pushLocationHashParams(this.props.location, this.props.history, { filters, skip: 0 });
    }
  }


  updateSorts(sorts, field) {
    const currentSortIndex = sorts.findIndex(s => s[0] == field);
    const newDir = currentSortIndex == -1 ? 'asc' : (sorts[currentSortIndex][1] == 'asc' ? 'desc' : false);
    const newSorts = sorts.slice();
    newSorts.splice(currentSortIndex, 1);
    if (!!newDir) {
      newSorts.unshift([field, newDir]);
    }
    util.pushLocationHashParams(this.props.location, this.props.history, {sorts: newSorts});
  }


  handleSelectedChange(changedItemIds, newSelectedStatus) {
    if (newSelectedStatus) this.props.onSelectedChange(this.props.selectedItemIds.union(changedItemIds));
    else this.props.onSelectedChange(this.props.selectedItemIds.subtract(changedItemIds))
  }
}


MultiView.propTypes = {
  className: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.string), PropTypes.string]),
  multiviewId: PropTypes.string.isRequired,
  collection: PropTypes.object.isRequired,
  subscriptionName: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,

  fields: PropTypes.arrayOf(PropTypes.shape({
    field: PropTypes.string.isRequired,
    label: PropTypes.string,
    description: PropTypes.string,
    sortable: PropTypes.bool,
    filter: PropTypes.shape({
      type: PropTypes.func,
      options: PropTypes.array,
      valueKey: PropTypes.string,
      labelKey: PropTypes.string,
      multi: PropTypes.bool,
    }),
    render: PropTypes.oneOfType([PropTypes.bool, PropTypes.func]),
    notSelectable: PropTypes.bool,
  })).isRequired,

  initialSorts: PropTypes.arrayOf(PropTypes.array),
  initialFilters: PropTypes.object,
  initialLimit: PropTypes.number,

  showPager: PropTypes.bool,
  showFilters: PropTypes.bool,
  showHeaders: PropTypes.bool,
  enableTextSearch: PropTypes.bool,
  textSearchPlaceholder: PropTypes.string,
  extraControls: PropTypes.node,
  selectorAlterations: PropTypes.func,
  onSelectedChange: PropTypes.func,
  selectedItemIds: PropTypes.instanceOf(Set),
};


MultiView.defaultProps = {
  initialLimit: 20,
  initialSkip: 0,
  initialFilters: {},
  initialSorts: [],
};


export default MultiView;
