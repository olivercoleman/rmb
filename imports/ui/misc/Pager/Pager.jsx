import React from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { ButtonToolbar, ButtonGroup, Button, Alert, DropdownButton, SplitButton, MenuItem } from 'react-bootstrap';

import './Pager.scss';

class Pager extends React.Component {
  constructor(props) {
    super(props);

    this.flip = this.flip.bind(this);
  }


  render() {
    const { skip, limit, itemCount, onLimitChange, disableLimitChange } = this.props;
    //if (!itemCount) return null;
    const limits = [1, 2, 5, 10, 20, 50, 100];
    return (
      <div className="Pager"><ButtonToolbar>
        <ButtonGroup bsSize="medium">
            <Button onClick={() => this.flip(-2)} disabled={skip == 0}>⏮</Button>
            <Button onClick={() => this.flip(-1)} disabled={skip == 0}>⯇</Button>
              <DropdownButton
                title={ (itemCount == 0 ? 0 : skip + 1) + " - " + Math.min(skip + limit, itemCount) + " / " + itemCount }
                id="pager-limit-select"
                disabled={disableLimitChange}
              >
                { limits.map(lim => (
                  <MenuItem onClick={ () => onLimitChange(lim) } key={lim}>
                    {lim}
                  </MenuItem>
                )) }
              </DropdownButton>
            <Button onClick={() => this.flip(1)} disabled={skip + limit >= itemCount}>⯈</Button>
            <Button onClick={() => this.flip(2)} disabled={skip + limit >= itemCount}>⏭</Button>
        </ButtonGroup>
      </ButtonToolbar></div>
    );
  }


  flip(direction) {
    const { skip, limit, itemCount, onPageChange } = this.props;
    let newSkip;
    switch(direction) {
      case -2:
        newSkip = 0;
        break;
      case -1:
        newSkip = skip - limit;
        break;
      case 1:
        newSkip = skip + limit;
        break;
      case 2:
        newSkip = itemCount-1;
        break;
    }

    if (newSkip >= itemCount) {
      newSkip -= limit;
    }
    if (newSkip < 0) {
      newSkip = 0;
    }

    newSkip = parseInt(newSkip / limit) * limit;

    if (skip != newSkip) {
      onPageChange(newSkip);
    }
  }
}


export default createContainer(({ collectionPagerId, skip, limit, onLimitChange, onPageChange, disableLimitChange }) => {
  return {
    itemCount: CollectionPagers.getTotalCount(collectionPagerId),
    skip,
    limit,
    onLimitChange,
    onPageChange,
    disableLimitChange,
  };
}, Pager);
