import React from 'react';
import autoBind from 'react-autobind';
import { Button } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';


export default class DeleteSelected extends React.Component {
  constructor(props) {
    super(props);
    autoBind(this);
  }


  render() {
    const { itemIds } = this.props;
    return (!itemIds || itemIds.size == 0 ? null :
      <Button className="btn-danger" onClick={this.deleteSelected}>Delete {itemIds.size}</Button>);
  }


  deleteSelected() {
    if (confirm('Are you sure you want to delete the selected items?')) {
      Meteor.call(this.props.removeMethod, [...this.props.itemIds.values()],
        (error) => {
          if (error) {
            Bert.alert(error.reason, 'danger');
          } else {
            this.props.onSuccess && this.props.onSuccess();
            Bert.alert('Items deleted!', 'success');
          }
        }
      );
    }
  }
}
