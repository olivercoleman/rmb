import React from 'react';
import classNames from 'classnames';

import './ExpandControl.scss';

const ExpandControl = ({expanded, onToggle, size, color}) => {
  const style = {
    fontSize: size || "150%",
  }
  if (color) style.color = color;

  return (
    <div className="chevron-circle-down"
      className={classNames("fa", "fa-chevron-circle-down", "ExpandControl", expanded ? 'expanded' : 'collapsed')}
      onClick={() => { if (onToggle) onToggle(!expanded) }}
      style={style}
    />
);
}

export default ExpandControl;
