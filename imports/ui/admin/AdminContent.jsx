import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import { ButtonToolbar, ButtonGroup, Button, Alert, DropdownButton, SplitButton, MenuItem, FormControl } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import _ from 'lodash';
import update from 'immutability-helper';
import { Set } from 'immutable';
import moment from 'moment';
import classNames from 'classnames';
import Select from 'react-select';
import { Bert } from 'meteor/themeteorchef:bert';
import SimpleSchema from 'simpl-schema';
import Access from 'meteor/ojc:access';
import { DateTime } from 'luxon';

import Content from '../../modules/content';
import util from '../util';
import MultiView from '../misc/MultiView/MultiView.jsx';
import Loading from '../misc/Loading/Loading.jsx';
import DeleteSelected from '../misc/DeleteSelected/DeleteSelected.jsx';

import './admincontent.scss';


const getValueFromFuncOrValue = funcOrValue => typeof funcOrValue == 'function' ? funcOrValue() : funcOrValue;


const renderTypeFunc = type => {
  if (type == Date) return v => DateTime.fromJSDate(v).toFormat("yyyy-LL-dd'T'HH:mmZZ");
  if (type == Object || type == Array) return v => {
    const output = JSON.stringify(v);
    if (typeof output == 'undefined') return "";
    if (output.length < 100) return output;
    return output.trim(100) + "...";
  }
  return String;
}


class AdminContent extends React.Component {
  constructor(props) {
    super(props);
    this.state ={
      selectedItemIds: Set(),
    }
    autoBind(this);
  }


  render() {
    const {
      history,
      location,
      collectionName,
      viewAnyAllowed,
      deleteAnyAllowed,
    } = this.props;

    const collection = Content.getTypeCollection(collectionName);

    const fields = Access.getFieldsForOp({op: 'view', schema: collection.schema}).map(field => {
      const schema = collection.schema[field];
      const isScalarOrString = [String, Number, SimpleSchema.Integer, Boolean].includes(schema.type);
      const isScalarOrStringOrDate = isScalarOrString || schema.type == Date;
      const def = {
        field,
        label: schema.label || field,
        sortable: isScalarOrStringOrDate,
      };

      if (schema.render) {
        def.render = schema.render;
      }
      else if (isScalarOrStringOrDate) {
        def.render = renderTypeFunc(schema.type);
      }

      // Filter def.
      // TODO handle array of scalars/strings
      if (isScalarOrStringOrDate) {
        def.filter = {
          type: schema.type,
        }

        if (schema.allowedValues) {
          const options = getValueFromFuncOrValue(schema.allowedValues);
          // Options may be a Set or Array so spread to array to use map.
          def.filter.options = [...options].map(v => {
            return {value: v, label: def.render ? def.render(v) : v}
          });
          def.filter.multi = true;
        }
        else if (schema.type == Boolean) {
          def.filter.options = [
            {value: false, label: 'false'},
            {value: true, label: 'true'},
          ];
        }

        if (schema.min) def.min = schema.min;
        if (schema.max) def.max = schema.max;
      }

      return def;
    });

    fields.push({
      label: 'Actions',
      field: '_actions',
      notSelectable: true,
      render: (val, item) =>
        <div>
          { deleteAnyAllowed && <Button onClick={() => this.handleRemove(item)} className="btn btn-danger" title="Delete this item">🗙</Button> }
        </div>
    });


    const extraControls = !!deleteAnyAllowed &&
      <DeleteSelected
        itemIds={this.state.selectedItemIds}
        removeMethod={collectionName + '.deleteMultiple'}
        onSuccess={() => this.setState({selectedItemIds: Set()})}
      />;


    return (
      <div className="AdminContent">
        <h3>Manage {collectionName}</h3>

        <MultiView
          className="table"
          multiviewId={collectionName}
          collection={collection}
          subscriptionName={ `${collectionName}.select${viewAnyAllowed ? 'Any' : 'Own'}` }
          history={history}
          location={location}
          fields={fields}
          onSelectedChange={selectedItemIds => this.setState({selectedItemIds})}
          selectedItemIds={this.state.selectedItemIds}
          showFilters={true}
          showPager={true}
          showHeaders={true}
          extraControls={extraControls}
        />
      </div>
    );
  }


  handleRemove(item) {
    if (confirm('Are you sure? This is permanent!')) {
      Meteor.call(this.props.collectionName + '.delete', item._id, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          this.setState({selectedItemIds: this.state.selectedItemIds.delete(item._id)})

          Bert.alert('Item deleted!', 'success');
        }
      });
    }
  }
}


export default createContainer(({ history, location, match }) => {
  const collectionName = match.params._type;
  const collection = Content.getTypeCollection(collectionName);
  return {
    history,
    location,
    collectionName,
    viewAnyAllowed: Access.allowed({rules: collection.access, op: 'view'}),
    deleteAnyAllowed: Access.allowed({rules: collection.access, op: 'delete'}),
  };
}, AdminContent);
